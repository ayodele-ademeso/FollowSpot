FROM python:3.7

COPY requirements.txt .
COPY secrets.sh .
COPY . .

RUN pip install -r requirements.txt
RUN . secrets.sh

EXPOSE 8080

CMD ["python3", "server.py"]
